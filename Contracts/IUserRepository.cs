﻿using System;
using Entities.Models;

namespace Contracts
{
    interface IUserRepository : IRepositoryBase<User>
    {
    }
}
