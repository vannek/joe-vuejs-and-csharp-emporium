﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using backend.Classes;
using Contracts;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Text;

namespace backend.Controllers
{

    public class Classes
    {
        public int count { get; set; }
        public List<DNDClass> results { get; set; }
    } 


    [Route("api/classes")]
    [ApiController]
    public class DNDPiggyBackController : ControllerBase
    {
        private ILoggerManager _logger;
        public DNDPiggyBackController(ILoggerManager logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<List<DNDClass>> Get()
        {
            List<DNDClass> classes = new List<DNDClass>(); 
            using(var client = new HttpClient())
            {
                var res = await client.GetAsync("http://www.dnd5eapi.co/api/classes/");
                var content = await res.Content.ReadAsStringAsync();
                Classes clsses = JsonConvert.DeserializeObject<Classes>(content);
                classes = clsses.results;

                _logger.LogDebug(clsses.ToString());
            }

            return classes;
        }

        public async Task<T> ParseJsonAsync<T>(string url)
        {
            T classRequest;
            try
            {
                using (var client = new HttpClient())
                {
                    var res = await client.GetAsync(url);
                    var content = await res.Content.ReadAsStringAsync();
                    classRequest = JsonConvert.DeserializeObject<T>(content);
                }
                return classRequest;
            } catch(Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet("{id:int:range(1,12)}")]
        public async Task<DNDClass> Get(int id)
        {
            return await ParseJsonAsync<DNDClass>("http://www.dnd5eapi.co/api/classes/"+id);
        }

        public async Task<List<DNDClass>> GetClasses()
        {
            List<DNDClass> classes = new List<DNDClass>();
            try
            {
                for(int i = 1; i <= 12; i++)
                {
                    var response = await GetResponseAsString(@"http://www.dnd5eapi.co/api/classes/{i}");
                    var newClass = JsonConvert.DeserializeObject<DNDClass>(response);
                    classes.Add(newClass);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return classes;
        }

        public async Task<string> GetResponseAsString(string endpoint)
        {
            string result = "";
            using (var client = new HttpClient())
            {
                var res = await client.GetAsync(endpoint);
                result = await res.Content.ReadAsStringAsync();
                _logger.LogDebug(result);
            }

            return result;
        }
    }
}