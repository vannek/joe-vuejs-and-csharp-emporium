﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Classes
{
    public class DNDClass
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("index")]
        public long Index { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("hit_die")]
        public long HitDie { get; set; }

        [JsonProperty("proficiency_choices")]
        public List<ProficiencyChoice> ProficiencyChoices { get; set; }

        [JsonProperty("proficiencies")]
        public List<Proficiency> Proficiencies { get; set; }

        [JsonProperty("saving_throws")]
        public List<Proficiency> SavingThrows { get; set; }

        [JsonProperty("starting_equipment")]
        public ClassLevels StartingEquipment { get; set; }

        [JsonProperty("class_levels")]
        public ClassLevels ClassLevels { get; set; }

        [JsonProperty("subclasses")]
        public List<Proficiency> Subclasses { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("count")]
        public long Count { get; set; }

        [JsonProperty("results")]
        public List<Proficiency> Results { get; set; }
    }
}
