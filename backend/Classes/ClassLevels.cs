﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Classes
{
    public class ClassLevels
    {
        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("class")]
        public string Class { get; set; }
    }
}
