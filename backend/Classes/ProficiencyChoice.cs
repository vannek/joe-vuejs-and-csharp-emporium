﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Classes
{
    public class ProficiencyChoice
    {
        [JsonProperty("from")]
        public List<Proficiency> From { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("choose")]
        public long Choose { get; set; }
    }
}
